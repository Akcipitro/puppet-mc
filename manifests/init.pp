class mc {
  package { 'java':
    ensure   => present,
    provider => 'yum',
  }

  user { 'minecraft':
    ensure => present,
    password => $facts['mc_user']['password'],
    shell => '/usr/sbin/nologin',
  }

  file { '/etc/mc':
    ensure  => directory,
    recurse => true,
    replace => false,
    source  => 'puppet:///modules/mc/mc',
  }

  $mc_config_hash = {
    'timestamp'                     => generate('/bin/date', '+%Y-%m-%d_%H:%M:%S'),
    'max_tick_time'                 => $facts['mc_config']['max_tick_time'],
    'generator_settings'            => $facts['mc_config']['generator_settings'],
    'force_gamemode'                => $facts['mc_config']['force_gamemode'],
    'allow_nether'                  => $facts['mc_config']['allow_nether'],
    'gamemode'                      => $facts['mc_config']['gamemode'],
    'enable_query'                  => $facts['mc_config']['enable_query'],
    'player_idle_timeout'           => $facts['mc_config']['player_idle_timeout'],
    'difficulty'                    => $facts['mc_config']['difficulty'],
    'spawn_monsters'                => $facts['mc_config']['spawn_monsters'],
    'op_permission_level'           => $facts['mc_config']['op_permission_level'],
    'pvp'                           => $facts['mc_config']['pvp'],
    'snooper_enabled'               => $facts['mc_config']['snooper_enabled'],
    'level_type'                    => $facts['mc_config']['level_type'],
    'hardcore'                      => $facts['mc_config']['hardcore'],
    'enable_command_block'          => $facts['mc_config']['enable_command_block'],
    'max_players'                   => $facts['mc_config']['max_players'],
    'network_compression_threshold' => $facts['mc_config']['network_compression_threshold'],
    'resource_pack_sha1'            => $facts['mc_config']['resource_pack_sha1'],
    'max_world_size'                => $facts['mc_config']['max_world_size'],
    'server_port'                   => $facts['mc_config']['server_port'],
    'server_ip'                     => $facts['mc_config']['server_ip'],
    'spawn_npcs'                    => $facts['mc_config']['spawn_npcs'],
    'allow_flight'                  => $facts['mc_config']['allow_flight'],
    'level_name'                    => $facts['mc_config']['level_name'],
    'view_distance'                 => $facts['mc_config']['view_distance'],
    'resource_pack'                 => $facts['mc_config']['resource_pack'],
    'displayname'                   => $facts['mc_config']['displayname'],
    'discoverability'               => $facts['mc_config']['discoverability'],
    'spawn_animals'                 => $facts['mc_config']['spawn_animals'],
    'white_list'                    => $facts['mc_config']['white_list'],
    'generate_structures'           => $facts['mc_config']['generate_structures'],
    'online_mode'                   => $facts['mc_config']['online_mode'],
    'max_build_height'              => $facts['mc_config']['max_build_height'],
    'level_seed'                    => $facts['mc_config']['level_seed'],
    'use_native_transport'          => $facts['mc_config']['use_native_transport'],
    'prevent_proxy_connections'     => $facts['mc_config']['prevent_proxy_connections'],
    'enable_rcon'                   => $facts['mc_config']['enable_rcon'],
    'motd'                          => $facts['mc_config']['motd'],
  }

  file { '/etc/mc/server.properties':
    content => epp('mc/server.properties.epp', $mc_config_hash)
  }

  $java_config_hash = {
    'javacmd'         => $facts['java_config']['javacmd'],
    'max_ram'         => $facts['java_config']['max_ram'],
    'java_parameters' => $facts['java_config']['java_parameters'],
  }

  file { '/etc/mc/settings-local.sh':
    content => epp('mc/settings-local.sh.epp', $java_config_hash)
  }

  file { '/etc/systemd/system/mc-server.service':
    source => 'puppet:///modules/mc/mc-server.service',
  }

  service { 'mc-server':
    ensure => running,
    enable => true,
  }
}
