/**
 * ------------------------------------------------------------
 *
 * This file is part of the FTB Presents Direwolf20 1.12 Modpack for Minecraft
 * Copyright (c) 2018 Feed the Beast LLC.
 *
 * All Rights Reserved unless otherwise explicitly stated.
 *
 * ------------------------------------------------------------
 */

/* Disable duplicate Bronze armour (Favouring Thermal Foundation) */
recipes.remove(<ic2:bronze_helmet>);
recipes.remove(<ic2:bronze_chestplate>);
recipes.remove(<ic2:bronze_leggings>);
recipes.remove(<ic2:bronze_boots>);

recipes.remove(<ic2:bronze_axe>);
recipes.remove(<ic2:bronze_hoe>);
recipes.remove(<ic2:bronze_pickaxe>);
recipes.remove(<ic2:bronze_shovel>);
recipes.remove(<ic2:bronze_sword>);

/* Fix broken bread recipe - How did this even happen? */
recipes.addShaped(<minecraft:bread> * 1, [[<ore:cropWheat>, <ore:cropWheat>, <ore:cropWheat>]]);

/* Remove uranium casting to prevent cross-mod breakage */
mods.tconstruct.Casting.removeBasinRecipe(<ic2:resource:10>, <liquid:uranium>);

/* Fix bug where casting cobalt blocks gives chisel block rather than tinkers' construct one */
mods.tconstruct.Casting.removeBasinRecipe(<chisel:blockcobalt>, <liquid:cobalt>);
mods.tconstruct.Casting.addBasinRecipe(<tconstruct:metal>, null, <liquid:cobalt>, 1296);

/* Fixed a duplication bug with Thermal Expansion and Ender IO */
mods.thermalexpansion.RedstoneFurnace.removeRecipe(<thermalfoundation:material:768>);

/* Remove Vanilla Prismarine from Chisel Groups due to duplication issues and the vanilla recipes not being equal to one another therefore making the Chiseling of these items unfair. */
mods.chisel.Carving.removeVariation("prismarine", <minecraft:prismarine:0>);
mods.chisel.Carving.removeVariation("prismarine", <minecraft:prismarine:1>);
mods.chisel.Carving.removeVariation("prismarine", <minecraft:prismarine:2>);

/* Helpful tooltips */
<tconstruct:toolforge>.addTooltip(format.yellow("Can be made from any metal block"));

/* This is not the easter egg you're looking for */
<ic2:misc_resource:3>.addTooltip(format.darkPurple("Keep out of reach of Soaryns"));

/* Fixed UniDict screwing with things causing bronze gears to not be craftable */
recipes.remove(<teslacorelib:gear_diamond>);
recipes.remove(<thermalfoundation:material:256>);
recipes.remove(<thermalfoundation:material:257>);
recipes.remove(<thermalfoundation:material:291>);
recipes.addShaped(<teslacorelib:gear_diamond> * 1, [
  [null, <ore:gemDiamond>],
  [<ore:gemDiamond>, <ore:ingotIron>, <ore:gemDiamond>],
  [null, <ore:gemDiamond>]
]);
recipes.addShaped(<thermalfoundation:material:256> * 1, [
  [null, <ore:ingotCopper>],
  [<ore:ingotCopper>, <ore:ingotIron>, <ore:ingotCopper>],
  [null, <ore:ingotCopper>]
]);
recipes.addShaped(<thermalfoundation:material:257> * 1, [
  [null, <ore:ingotTin>],
  [<ore:ingotTin>, <ore:ingotIron>, <ore:ingotTin>],
  [null, <ore:ingotTin>]
]);
recipes.addShaped(<thermalfoundation:material:291> * 1, [
  [null, <ore:ingotBronze>],
  [<ore:ingotBronze>, <ore:ingotIron>, <ore:ingotBronze>],
  [null, <ore:ingotBronze>]
]);

/* Fix cheap diamond recipe caused by mod cross compatibility */
mods.thermalexpansion.Compactor.removePressRecipe(<ic2:crafting:17>);

/* Industrial Foregoing - Laser Drill compatibility for other mods */
mods.industrialforegoing.LaserDrill.add(8, <appliedenergistics2:quartz_ore>, 2); /* Applied Energistics 2 */
mods.industrialforegoing.LaserDrill.add(8, <appliedenergistics2:charged_quartz_ore>, 1); /* Applied Energistics 2 */
mods.industrialforegoing.LaserDrill.add(15, <actuallyadditions:block_misc:3>, 6); /* Actually Additions */
mods.industrialforegoing.LaserDrill.add(3, <forestry:resources>, 6); /* Forestry */
mods.industrialforegoing.LaserDrill.add(3, <thermalfoundation:ore:8>, 1); /* Thermal Foundation */

/* Fix an exploit allowing you to get leather with a Thermal Expansion Sawmill and Pam's HarvestCraft Woven Cotton */
recipes.remove(<minecraft:leather_helmet>);
recipes.addShaped(<minecraft:leather_helmet> * 1, [
  [<ore:leather>, <ore:leather>, <ore:leather>],
  [<ore:leather>, null, <ore:leather>]
]);
recipes.remove(<minecraft:leather_chestplate>);
recipes.addShaped(<minecraft:leather_chestplate> * 1, [
  [<ore:leather>, null, <ore:leather>],
  [<ore:leather>, <ore:leather>, <ore:leather>],
  [<ore:leather>, <ore:leather>, <ore:leather>]
]);
recipes.remove(<minecraft:leather_leggings>);
recipes.addShaped(<minecraft:leather_leggings> * 1, [
  [<ore:leather>, <ore:leather>, <ore:leather>],
  [<ore:leather>, null, <ore:leather>],
  [<ore:leather>, null, <ore:leather>]
]);
recipes.remove(<minecraft:leather_boots>);
recipes.addShaped(<minecraft:leather_boots> * 1, [
  [<ore:leather>, null, <ore:leather>],
  [<ore:leather>, null, <ore:leather>]
]);
